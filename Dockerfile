FROM node
ENV DEBUG *
WORKDIR /app
ADD . /app
RUN npm install
CMD ./bin/www